package com.example.androidtema1.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.androidtema1.Fragments.F1A2
import com.example.androidtema1.Fragments.F2A2
import com.example.androidtema1.R
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_f1_a2.*

class SecondActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val fragment = F1A2.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frame_layout_new, fragment, "F1A2" )
        transaction.commit()

    }

   override fun onBackPressed() {
        finishAffinity()
    }

}
