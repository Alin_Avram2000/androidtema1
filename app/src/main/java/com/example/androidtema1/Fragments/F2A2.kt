package com.example.androidtema1.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.androidtema1.R


class F2A2 : Fragment() {
    companion object {
        fun newInstance() = F2A2()
    }
    private var button1 :Button ?= null
    private var button2 :Button ?= null
    private var button3 :Button ?= null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_f2_a2, container, false)
        button1 = view.findViewById(R.id.btn1)
        button2 = view.findViewById(R.id.btn2)
        button3 = view.findViewById(R.id.btn3)
        button1?.setOnClickListener {
            val fragment = F3A2.newInstance()
            val manager = activity?.supportFragmentManager
            val transaction = manager?.beginTransaction()
            transaction?.add(R.id.frame_layout_new,fragment, "F3A2" )
            manager?.popBackStack()
            transaction?.addToBackStack("F3A2")
            transaction?.commit()
        }
        button2?.setOnClickListener {
            removeFrag()
        }
        button3?.setOnClickListener {
            activity?.finish()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun removeFrag()
    {

        val manager: FragmentManager = activity!!.supportFragmentManager
        val trans: FragmentTransaction = manager.beginTransaction()
        val fragment = activity!!.supportFragmentManager.findFragmentByTag("F1A2");
        if (fragment != null) {
            trans.remove(fragment)
        }
        trans.commit()
    }
}

