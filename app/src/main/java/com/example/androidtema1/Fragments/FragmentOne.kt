package com.example.androidtema1.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.androidtema1.Activities.MainActivity
import com.example.androidtema1.Activities.SecondActivity
import com.example.androidtema1.R
import kotlinx.android.synthetic.main.activity_fragment_one.*
import android.content.Intent
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_f1_a2.*

class FragmentOne : Fragment() {

    private var button :Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_fragment_one, container, false)
        button = view.findViewById(R.id.btn_go_to_second_activity)
        button?.setOnClickListener {
            goToSecondActivity()
        }
        return view
    }

    fun goToSecondActivity() {
        val intent = Intent(activity, SecondActivity::class.java)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }





}
