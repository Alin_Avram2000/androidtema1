package com.example.androidtema1.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.androidtema1.R
import kotlinx.android.synthetic.main.activity_fragment_one.*
import kotlinx.android.synthetic.main.fragment_f1_a2.*
import kotlinx.android.synthetic.main.fragment_f1_a2.view.add_f2a2


class F1A2 : Fragment() {
    companion object {
        fun newInstance() = F1A2()
    }

    private var button :Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_f1_a2, container, false)
        button = view.findViewById(R.id.add_f2a2)
        button?.setOnClickListener {
            addFragment2()
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun addFragment2() {
        val fragment = F2A2.newInstance()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frame_layout_new, fragment, "F2A2")
        transaction?.addToBackStack("F2A2")
        transaction?.commit()
    }
}
